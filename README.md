# YDLIDAR

Demo program in C# for connecting to a YDLIDAR and accessing its data.

Plotting the data in Excel using a radar plot works well to visualise the data captured

It is still noiser than I would like it to be.

Currently optimised for static scenes as it accumulates multiple scans ... this could be easily adapted to just take the raw data.