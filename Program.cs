﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Diagnostics;

namespace ConsoleApp2
{
    class Program
    {
        static int scale = 1;

        static uint ToInt(byte a, byte b)
        {
            return (((uint)b) << 8) + a;
        }
        static uint TwoBytes(byte a, byte b)
        {
            return (((uint)a) << 8) + b;
        }

        static uint ToInt(byte a, byte b, byte c, byte d, byte highBtyeMask = 0xFF)
        {
            return (((uint)(d & highBtyeMask)) << 24) + (((uint)c) << 16) + (((uint)b) << 8) + a;
        }

        static void Dump(byte[] buffer, int count)
        {
            for (int i = 0; i < count / 16; i++)
            {
                string value = BitConverter.ToString(buffer, i * 16, 16);
                value = value.Replace("-", "");
                Console.WriteLine("{0}", value);
            }
            if (count % 16 != 0)
            {
                string value = BitConverter.ToString(buffer, (count / 16) * 16, count % 16);
                value = value.Replace("-", "");
                Console.WriteLine("{0}", value);
            }
        }

        static byte[] ReadStream(SerialPort p)
        {
            byte[] buffer = new byte[4096];
            int read = 0;

            int tries = 0;
            while (p.BytesToRead < 10 && tries < 20)
            {
                Thread.Sleep(100);
                tries++;
            }

            if (p.BytesToRead == 0) return buffer;

            if (p.BytesToRead >= 10)
            {
                bool first = true;
                while (buffer[0] != 0xAA)
                {
                    //if (!first) Console.WriteLine("Scanning for header.");
                    buffer[0] = (byte)p.ReadByte();
                    first = false;
                }

                try
                {
                    read = p.Read(buffer, 1, 9) + 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                if (read == 10 && buffer[0] == 0xaa && buffer[1] == 0x55)
                {
                    read = 0;
                    int len = buffer[3] * 2;
                    if (len > 0)
                    {
                        while (p.BytesToRead < len && tries < 20)
                        {
                            Thread.Sleep(100);
                            tries++;
                        }

                        if (p.BytesToRead >= len)
                        {
                            read = 0;
                            try
                            {
                                read = p.Read(buffer, 10, len);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            if (read == len)
                            {
                                //Dump(buffer, read + 10);
                            }
                            else
                            {
                                //Console.WriteLine("Serial read failed {0} bytes read when {1} expected.", read, len);
                                if (read > 0)
                                {
                                    //Dump(buffer, read + 10);
                                }
                            }
                        }
                        else
                        {
                            //Console.WriteLine("Serial read failed when {0} expected but only {1} arrived.", len, p.BytesToRead);
                        }
                    }
                }
                else
                {
                    //Console.WriteLine("Serial read failed {0} bytes read when 10 expected.", read);
                    if (read > 0)
                    {
                        //Dump(buffer, read);
                    }
                }
            }
            else
            {
                //Console.WriteLine("Serial read failed {0} available read when 10 expected.", p.BytesToRead);
            }
            return buffer;
        }

        static void ReadUntilEnd(SerialPort p)
        {
            byte[] buffer = new byte[4096];
            int lastAvail = -1;
            while (p.BytesToRead != lastAvail)
            {
                lastAvail = p.BytesToRead;
                Thread.Sleep(200);
            }
            p.Read(buffer, 0, p.BytesToRead);
            Dump(buffer, lastAvail);
            for (int i =0; i < lastAvail; i++)
            {
                Console.Write("{0}", (char)buffer[i]);
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        static byte[] Read(SerialPort p)
        {
            byte[] buffer = new byte[4096];
            int read = 0;

            int tries = 0;
            while (p.BytesToRead < 7 && tries < 20)
            {
                Thread.Sleep(100);
                tries++;
            }

            if (p.BytesToRead >= 7)
            {
                try
                {
                    read = p.Read(buffer, 0, 7);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                if (read == 7 && buffer[0] == 0xa5 && buffer[1] == 0x5a)
                {
                    int mode = (buffer[5] & 0xC0) >> 6;

                    if (mode == 0)
                    {
                        uint len = ToInt(buffer[2], buffer[3], buffer[4], buffer[5], 0x3F);
                        if (len > 0)
                        {
                            tries = 0;
                            while (p.BytesToRead < len && tries < 20)
                            {
                                Thread.Sleep(100);
                                tries++;
                            }

                            if (p.BytesToRead >= len)
                            {
                                read = 0;
                                try
                                {
                                    read = p.Read(buffer, 7, (int)len);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                                if (read == len)
                                {
                                    Dump(buffer, read + 7);
                                }
                                else
                                {
                                    Console.WriteLine("Serial read failed {0} bytes read when {1} expected.", read, len);
                                    if (read > 0)
                                    {
                                        Dump(buffer, read + 7);
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("Serial read failed when {0} expected but only {1} arrived.", len, p.BytesToRead);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Serial read failed {0} bytes read when 7 expected.", read);
                    if (read > 0)
                    {
                        Dump(buffer, read);
                    }
                }
            }
            else
            {
                Console.WriteLine("Serial read failed {0} available read when 7 expected.", p.BytesToRead);
            }

            return buffer;
        }

        static void Info(SerialPort p)
        {
            Console.WriteLine("Info");
            byte[] info = { 0xa5, 0x90 };
            p.Write(info, 0, info.Count());
            byte[] buffer = Read(p);
            Console.WriteLine("   Type Code [4]: {0}", buffer[6]);
            int mode = (buffer[5] & 0xC0) >> 6;
            string modes = DecodeMode(mode);
            Console.WriteLine("   Mode: {0} ({1})", mode, modes);
            Console.WriteLine("   Model: {0}", buffer[7]);
            Console.WriteLine("   Firmware: {0}.{1}", buffer[9], buffer[8]);
            Console.WriteLine("   Hardware: {0}", buffer[10]);
            Console.Write("   Serial: ");
            for (int i = 0; i < 16; i++)
            {
                Console.Write("{0}", buffer[11 + i]);
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        static string DecodeMode(int mode)
        {
            if (mode == 0) return "Single Response";
            if (mode == 1) return "Continuous Response";
            return "Undefined";
        }

        static void Health(SerialPort p)
        {
            Console.WriteLine("Health Status");
            byte[] health = { 0xa5, 0x91 };
            p.Write(health, 0, health.Count());
            byte[] buffer = Read(p);
            Console.WriteLine("   Type Code [6]: {0}", buffer[6]);
            int mode = (buffer[5] & 0xC0) >> 6;
            string modes = DecodeMode(mode);
            Console.WriteLine("   Mode: {0} ({1})", mode, modes);
            string status = "Unknown";
            switch(buffer[7])
            {
                case 0:
                    status = "Running Normal";
                    break;
                case 1:
                    status = "Running";
                    break;
                case 2:
                    status = "Running Incorrectly";
                    break;
            }
            Console.WriteLine("   Status Code: {0} ({1})", buffer[7], status);
            uint error = ToInt(buffer[8], buffer[9]);
            string errors;
            if (error == 0) errors = "Ok";
            else errors = "ERROR!";
            Console.WriteLine("   Error Code: {0} ({1})", error, errors);
            Console.WriteLine();
        }

        static void Start(SerialPort p)
        {
            Console.WriteLine("Start");
            byte[] start = { 0xa5, 0x60 };
            p.Write(start, 0, start.Count());
            byte[] buffer = Read(p);
            Console.WriteLine("   Type Code [129]: {0}", buffer[6]);
            int mode = (buffer[5] & 0xC0) >> 6;
            string modes = DecodeMode(mode);
            Console.WriteLine("   Mode: {0} ({1})", mode, modes);
            Console.WriteLine();
        }

        static double ToRadians(double d)
        {
            return (d * 2 * Math.PI) / 360;
        }

        static double ToDegrees(double d)
        {
            return (d * 360) / (2 * Math.PI);
        }

        static int Normalise(double a)
        {
            while (a < 0) a += 360;
            while (a >= 360) a -= 360;
            return (int)(a*scale);
        }

        static void Main(string[] args)
        {
            double[] distances = new double[360*scale];
            int[] samples = new int[360*scale];
            SerialPort p = new SerialPort("COM3", 128000, Parity.None, 8, StopBits.One);
            p.ReadTimeout = 100;
            p.Handshake = Handshake.XOnXOff;
            p.DtrEnable = true;

            p.Open();

            if (p.IsOpen)
            {
                Console.WriteLine("Reboot");
                byte[] reboot = { 0xa5, 0x80 };
                p.Write(reboot, 0, reboot.Count());
                ReadUntilEnd(p);

                Info(p);

                Health(p);

                Start(p);

                int ok = 0;
                int notok = 0;
                int samplesSinceDump = 0;
                DateTime startTime = DateTime.Now;
                while ((DateTime.Now - startTime).TotalSeconds < 20)
                {
                    byte[] buffer = ReadStream(p);
                    //Console.WriteLine("Scan");
                    string type = "Unknown";
                    switch(buffer[2])
                    {
                        case 0:
                            type = "Point Cloud";
                            break;
                        case 1:
                            type = "Zero";
                            break;
                    }
                    //Console.WriteLine("   Packet Type: {0} ({1})", buffer[2], type);
                    //Console.WriteLine("   Quantity: {0}", buffer[3]);
                    double startAngle = ((double)(ToInt(buffer[4], buffer[5]) >> 1)) / 64.0;
                    double endAngle = ((double)(ToInt(buffer[6], buffer[7]) >> 1)) / 64.0;
                    //Console.WriteLine("   Start Angle: {0}", startAngle);
                    //Console.WriteLine("   End Angle: {0}", endAngle);
                    //Console.WriteLine("   Check Code: {0}", ToInt(buffer[8], buffer[9]));

                    double startAngleCorrect = 0;
                    double distanceStart = ((double)ToInt(buffer[10], buffer[11])) / 4.0;
                    if (distanceStart > 0)
                    {
                        startAngleCorrect = ToDegrees(Math.Atan(ToRadians(21.8 * (155.3 - distanceStart) / (155.3 + distanceStart))));
                    }

                    double endAngleCorrect = 0;
                    double distanceEnd = ((double)ToInt(buffer[10 + (buffer[3]-1) * 2], buffer[11 + (buffer[3] - 1) * 2])) / 4.0;
                    if (distanceEnd > 0)
                    {
                        endAngleCorrect = ToDegrees(Math.Atan(ToRadians(21.8 * (155.3 - distanceEnd) / (155.3 + distanceEnd))));
                    }
                    startAngle += startAngleCorrect;
                    endAngle += endAngleCorrect;

                    uint check = TwoBytes(buffer[0], buffer[1]);
                    //Console.WriteLine("{0}", check);
                    for (int i = 1; i < buffer[3] + 5; i++)
                    {
                        check = check ^ TwoBytes(buffer[i * 2], buffer[i * 2 + 1]); // S1 .. SN
                        //Console.WriteLine("{0}", check);
                    }

                    if (check != 0)
                    {
                        //Console.WriteLine("   Calculated Check - This should be zero: {0}", check);
                        //Dump(buffer, 10 + buffer[3] * 2);
                        notok++;
                    }
                    else { ok++; }

                    if (check == 0)
                    {
                        double perSampleAngle = 0;
                        if (buffer[3]> 1) perSampleAngle = (endAngle - startAngle) / (buffer[3]-1);
                        for (int i = 0; i < buffer[3]; i++)
                        {
                            double distance = ((double)ToInt(buffer[10 + i * 2], buffer[11 + i * 2])) / 4.0;

                            double angle = startAngle + i * perSampleAngle;
                            int normalisedAngle = Normalise(angle);
                            if (distance > 0)
                            {
                                distances[normalisedAngle] += distance;
                                samples[normalisedAngle]++;
                            }
                            samplesSinceDump++;

                            //Console.WriteLine("      Sample: {0:F3} = {1:F1}mm", angle, distance);
                        }
                    }
                    //else
                    //{
                        //Console.WriteLine("Packet corrupted ... data ignored.");
                    //}
                    //Console.WriteLine();
                }

                p.DtrEnable = false;

                Console.WriteLine("OK : {0}, Not OK : {1}", ok, notok);

                for (int j = 0; j < 360 * scale; j++)
                {
                    if (samples[j] > 0)
                    {
                        int scaleDiff = 2;
                        if (j != 0 && j != 360 * scale - 1)
                        {
                            double diffPrior = Math.Abs(distances[j] / samples[j] - distances[j - 1] / samples[j-1]);
                            double diffNext = Math.Abs(distances[j] / samples[j] - distances[j + 1] / samples[j+1]);
                            double diffNextPrior = Math.Abs(distances[j-1] / samples[j-1] - distances[j + 1] / samples[j+1]);
                            if (diffNextPrior * scaleDiff < diffPrior && diffNextPrior * scaleDiff < diffNext)
                            {
                                distances[j] = (distances[j - 1] / samples[j - 1] + distances[j + 1] / samples[j+1]) / 2;
                                samples[j] = 1;
                            }
                        }

                        Console.WriteLine("   {0:F1}:{1:F3}", (double)j/scale, distances[j] / samples[j]);
                    }
                    distances[j] = 0;
                    samples[j] = 0;
                }

                //Health(p);

                Console.WriteLine("Stop");
                byte[] stop = { 0xa5, 0x65 };
                p.Write(stop, 0, stop.Count());

                p.Close();

                Debug.Assert(false);
            }
        }
    }
}
